<?php

/**
 * @file
 * Webform option field API integration.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\webform\WebformSubmissionForm;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Implements hook_webform_options_alter().
 *
 * Get allowed values from specified entity type id and field name.
 */
function webform_options_field_api_webform_options_alter(array &$options, array &$element, $options_id = NULL) {
  // Check for double underscore ENTITY_TYPE to FIELD_NAME delimiter.
  if (strpos($options_id, '__') === FALSE) {
    return;
  }

  // Get ENTITY_TYPE_ID__FIELD_NAME from options ID.
  // For example, you would use 'node__field_shared_states' to get the
  // options (aka allowed values) from a content type's field_shared_states.
  list($entity_type_id, $field_name) = explode('__', $options_id);

  // Get the field's storage and exit if the fields storage does not exist.
  $field_storage = FieldStorageConfig::loadByName($entity_type_id, $field_name);
  if (!$field_storage) {
    return;
  }

  // Get field's allowed values.
  $options += $field_storage->getSetting('allowed_values');

  // Store a reference to the webform options id.
  $element['#webform_options_field_api__entity_type_id'] = $entity_type_id;
  $element['#webform_options_field_api__field_name'] = $field_name;
}

/**
 * Implements hook_webform_element_alter().
 *
 * Limit allowed values to webform submission's source entity's selected values.
 *
 */
function webform_options_field_api_webform_element_alter(array &$element, FormStateInterface $form_state, array $context) {
  if (!isset($element['#webform_options_field_api__entity_type_id'])) {
    return;
  }

  $entity_type_id = $element['#webform_options_field_api__entity_type_id'];
  $field_name = $element['#webform_options_field_api__field_name'];
  $element_key = $element['#webform_key'];

  /** @var \Drupal\webform\WebformSubmissionForm $form_object */
  $form_object = $form_state->getFormObject();
  if (!$form_object instanceof WebformSubmissionForm) {
    return;
  }

  /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
  $webform_submission = $form_object->getEntity();
  $source_entity = $webform_submission->getSourceEntity();
  if (!$source_entity || $source_entity->getEntityTypeId() !== $entity_type_id || !method_exists($source_entity, 'hasField') || !$source_entity->hasField($field_name)) {
    return;
  }

  // Get the selected options from the source entity's field.
  $source_entity_options = [];
  foreach ($source_entity->$field_name as $item) {
    if (!_webform_option_field_api_is_option_submitted($webform_submission, $element_key, $item->value)) {
      $source_entity_options[$item->value] = $item->value;
    }
  }
  if (empty($source_entity_options)) {
    // @todo For now display all options.
    // @todo Decide if the form should be disabled. Requires a hook_form_alter().
    return;
  }

  // Make sure to always include the default value as an option.
  if (isset($element['#default_value'])) {
    if (is_array($element['#default_value'])) {
      $source_entity_options[$element['#default_value']] = $element['#default_value'];
    }
    else {
      $source_entity_options += array_combine($element['#default_value'], $element['#default_value']);
    }
  }

  // DEBUG:
  // dsm($options); dsm($options_filter);

  // Limit the Webform's options to source entity's selected options.
  $element['#options'] = array_intersect_key($element['#options'], $source_entity_options);
}

/**
 * Determine if element option value is selected.
 *
 * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
 *   A webform submission.
 * @param string $name
 *   Element name
 * @param string $value
 *   Element value
 *
 * @return boolean
 *   TRUE if element option value is selected.
 *
 * @see \Drupal\webform\Plugin\WebformElementBase::validateUnique
 */
function _webform_option_field_api_is_option_submitted(WebformSubmissionInterface $webform_submission, $name, $value) {
  $webform = $webform_submission->getWebform();

  $query = \Drupal::database()->select('webform_submission', 'ws');
  $query->leftJoin('webform_submission_data', 'wsd', 'ws.sid = wsd.sid');
  $query->fields('ws', ['sid']);
  $query->condition('wsd.webform_id', $webform->id());
  $query->condition('wsd.name', $name);
  $query->condition('wsd.value', $value);
  if ($source_entity = $webform_submission->getSourceEntity()) {
    $query->condition('ws.entity_type', $source_entity->getEntityTypeId());
    $query->condition('ws.entity_id', $source_entity->id());
  }
  else {
    $query->isNull('ws.entity_type');
    $query->isNull('ws.entity_id');
  }

  // Exclude the current webform submission.
  if ($sid = $webform_submission->id()) {
    $query->condition('ws.sid', $sid, '<>');
  }

  // Using range() is more efficient than using countQuery() for data checks.
  $query->range(0, 1);
  return $query->execute()->fetchField() ? TRUE : FALSE;
}
